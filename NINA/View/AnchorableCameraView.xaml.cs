﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableCameraView.xaml
    /// </summary>
    public partial class AnchorableCameraView : UserControl {

        public AnchorableCameraView() {
            InitializeComponent();
        }
    }
}