﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for MinimalisticGuiderGraph.xaml
    /// </summary>
    public partial class MinimalisticGuiderGraph : UserControl {

        public MinimalisticGuiderGraph() {
            InitializeComponent();
        }
    }
}