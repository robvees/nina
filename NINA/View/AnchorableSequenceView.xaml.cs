﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableSequenceView.xaml
    /// </summary>
    public partial class AnchorableSequenceView : UserControl {

        public AnchorableSequenceView() {
            InitializeComponent();
        }
    }
}