﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for ApplicationMenu.xaml
    /// </summary>
    public partial class ApplicationMenu : UserControl {

        public ApplicationMenu() {
            InitializeComponent();
        }
    }
}