﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableImageHistoryView.xaml
    /// </summary>
    public partial class AnchorableImageHistoryView : UserControl {

        public AnchorableImageHistoryView() {
            InitializeComponent();
        }
    }
}