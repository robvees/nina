﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorablePlateSolveView.xaml
    /// </summary>
    public partial class AnchorablePlateSolveView : UserControl {

        public AnchorablePlateSolveView() {
            InitializeComponent();
        }
    }
}