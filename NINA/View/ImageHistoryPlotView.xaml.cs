﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for ImageHistoryPlotView.xaml
    /// </summary>
    public partial class ImageHistoryPlotView : UserControl {

        public ImageHistoryPlotView() {
            InitializeComponent();
        }
    }
}